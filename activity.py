class Camper():
  def __init__(self, attribute_name, batch, course_type):
    self.attribute_name = attribute_name
    self.batch = batch
    self.course_type = course_type
  
  def career_track(self):
    print(f"Currently enrolled in the {self.course_type} program")
  def info(self):
    print(f"My Name is {self.attribute_name} of batch {self.batch}")
zuitt_camper = Camper("Jan Michael", "303", "Python Short Course")
print(f"Camper Name: {zuitt_camper.attribute_name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")

zuitt_camper.info()
zuitt_camper.career_track()
